const dialogflow = require('@google-cloud/dialogflow');
const uuid = require('uuid');
const keyFilename = 'C:\\xampp\\htdocs\\Code\\wasap-chatbot\\credentials-sa-dialogflow.json'
const projectId = 'desa-owner-poc-chatbot1-tlsdqs'
async function runSample(message) {
    const sessionId = uuid.v4();
    const sessionClient = new dialogflow.SessionsClient({
        keyFilename
    });
    const sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                text: message,
                languageCode: 'es-ES',
            },
        },
    };
    const responses = await sessionClient.detectIntent(request);
    const result = responses[0].queryResult;
    console.log('--------------------------------')
    console.log(`  Query: ${result.queryText}`);
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
        console.log(`  Intent: ${result.intent.displayName}`);
    } else {
        console.log(`  No intent matched.`);
    }
    console.log('--------------------------------')
    return result.fulfillmentText;
}
//TEST
// runSample('Hola')
exports.runSample = runSample