const wa = require('@open-wa/wa-automate');
const dialogflowQuery = require('./dialogflow');
wa.create().then(client => start(client));

async function start(client) {
    client.onMessage(async message => {
        const sender = message.sender.name;
        if (message.mimetype) {
            const filename = `${message.t}.${mime.extension(message.mimetype)}`;
            console.log(filename);
            const mediaData = await decryptMedia(message);
            console.log(mediaData);
        }

        const intent = await dialogflowQuery.runSample(message.body);
        if (sender === 'Mathias Velilla' && message.body !== '' && message.body !== null && message.body !== undefined &&
            message.chat.name !== 'Ventas y Trueques����🤝' &&
            message.chat.name !== 'Comunidad Villa MHdS') {
            client.sendText(message.from, intent);
        }
    });
}